function authorize() {
  let query = {
    client_id: CLIENT_ID,
    redirect_uri: REDIRECT_URI,
    response_type: "code",
    scope: '',
    state: ''
  }
  
  let authorizationUrl = HOST + "/oauth/authorize?" + new URLSearchParams(query);

  window.location.href = authorizationUrl;
}