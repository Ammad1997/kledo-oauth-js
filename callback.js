function callback() {
  let url = new URL(window.location.href);
  let CODE = url.searchParams.get("code");
  let body = JSON.stringify({
    grant_type: "authorization_code",
    code: CODE,
    redirect_uri: REDIRECT_URI,
    client_id: CLIENT_ID,
    client_secret: CLIENT_SECRET,
  });
  let option = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: body,
  }

  fetch(HOST + "/oauth/token", option)
    .then((res) => res.json())
    .then((data) => {
      localStorage.setItem("token_type", data.token_type);
      localStorage.setItem("access_token", data.access_token);
      localStorage.setItem("refresh_token", data.refresh_token);
      localStorage.setItem("expires_in", data.expires_in);

      window.location.href = "/";
    });
}

window.addEventListener("load", callback);
