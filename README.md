## Membuat OAuth2 App Client

Untuk mendapatkan `client id` dan `client secret`, ikuti langkah-langkah berikut untuk membuat OAuth Client.

* Buat akun di [kledo](https://kledo.com/daftar/) (Jika belum punya)
* Login ke [Kledo](https://app.kledo.com/)
* Buka menu `Pengaturan` > `Integrasi Aplikasi`
* Kemudian klik tombol `Tambah`
* Masukkan `Nama Aplikasi` dan `OAuth 2.0 redirect URI`
* Klik tombol `Simpan`
* Copy `client id` dan `client secret` kemudian simpan ke dalam text file.


## Cara menggunakan kode ini
* Rename `env.js.example` menjadi `env.js`
* Sesuaikan data di `env.js` dengan data perusahaan Anda
* Jalankan code ini dengan menggunakan server misal [live-server vscode](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)
* Untuk authorisasi silakan kunjungi halaman [authorize.html](http://127.0.0.1:5500/authorize.html)
* Kemudian klik tombol authorize
* Selanjutnya Anda akan diminta login dan konfrimasi
* Terakhir Anda Akan diredirect lagi ke halaman index dan bisa menggunakan form untuk call api KLEDO
* Untuk data login authorisasi akan di simpan di localstorage [screenshoot](https://kledo-live-user.s3.ap-southeast-1.amazonaws.com/majujaya12.api.kledo.com/pos/temp/240305/OpchBAS8wBXoNJfexCf2.png)
* Untuk me-refresh token (jika token expired) kunjungi halaman [refresh.html](http://127.0.0.1:5500/authorize.html)

**Semoga Dapat Membantu**