function refresh() {
  let refreshToken = localStorage.getItem("refresh_token");

  let body = JSON.stringify({
    grant_type: "refresh_token",
    refresh_token: refreshToken,
    client_id: CLIENT_ID,
    client_secret: CLIENT_SECRET,
    scope: "",
  });

  let option = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: body,
  };

  fetch(HOST + "/oauth/token", option)
    .then((res) => res.json())
    .then((data) => {
      localStorage.setItem("token_type", data.token_type);
      localStorage.setItem("access_token", data.access_token);
      localStorage.setItem("refresh_token", data.refresh_token);
      localStorage.setItem("expires_in", data.expires_in);

      window.location.href = "/";
    });
}

window.addEventListener("load", refresh);
