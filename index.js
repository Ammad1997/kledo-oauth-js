document.querySelector(
  "#api-form",
  addEventListener("submit", (event) => {
    event.preventDefault();

    let method = document.querySelector("#method").value;
    let url = document.querySelector("#url").value;
    let body = document.querySelector("#body").value;

    let option = {
      method: method,
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + document.querySelector("#access_token").value,
      },
    };

    if (method === "POST") {
      option.body = body;
    }

    fetch(HOST + url, option)
      .then((res) => res.json())
      .then((data) => {
        document.querySelector("#res-data").value = JSON.stringify(data, null, 2);
      });
  })
);

document.addEventListener("DOMContentLoaded", () => {
  if (localStorage.getItem("access_token")) {
    document.querySelector("#access_token").value = localStorage.getItem(
      "access_token"
    );
  }

  if (localStorage.getItem("refresh_token")) {
    document.querySelector("#refresh_token").value = localStorage.getItem(
      "refresh_token"
    );
  }

  if (localStorage.getItem("expires_in")) {
    document.querySelector("#expires_in").value = localStorage.getItem(
      "expires_in"
    );
  }

  document.querySelector("#base-url").appendChild(document.createTextNode(HOST));
});